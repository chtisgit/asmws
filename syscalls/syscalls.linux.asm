
stdin = 0
stdout = 1
stderr = 2

SYS_EXIT = 60
SYS_WRITE = 1
SYS_MMAP = 9
SYS_MUNMAP = 10
SYS_SOCKET = 41
SYS_READ = 0
SYS_SENDFILE = 40
SYS_BIND = 49
SYS_LISTEN = 50
SYS_ACCEPT = 43
SYS_CLOSE = 3
SYS_FORK = 57
SYS_PIPE = 22
SYS_WAIT4 = 61
SYS_SETREUID = 113
SYS_SETREGID = 114
SYS_GETUID = 102
SYS_GETGID = 104
SYS_POLL = 7

PF_INET6 = 10
SOCK_STREAM = 1
PROT_READ = 1 
PROT_WRITE = 2
PROT_EXEC = 4
PROT_NONE = 0
MAP_SHARED = 1
MAP_PRIVATE = 2
MAP_ANONYMOUS = 0x20
MAP_FAILED = -1
POLLIN = 0x1
POLLPRI = 0x2
POLLOUT = 0x4 
POLLERR = 0x8
POLLHUP = 0x10
POLLNVAL = 0x20

struc sockaddr_in6 Family, Port, Flowinfo, ScopeID, [Addr]{
common
	.sin6_family dw Family
	.sin6_port dw ((Port and 0xFF) shl 8) or ((Port shr 8) and 0xFF)
	.sin6_flowinfo dd Flowinfo
forward
	.sin6_addr db Addr
common
	.sin6_scope_id dd ScopeID
}
sizeof_sockaddr_in6 = 28

struc pollfd fd, events, revents {
	.fd dd fd
	.events dw events
	.revents dw revents
}
sizeof_pollfd = 8

macro Syscall Call
{
	mov rax, Call
	syscall
}

macro ExitSuccess
{
	xor rdi, rdi
	Syscall SYS_EXIT
}

macro ExitFailure
{
	mov rdi, 1
	Syscall SYS_EXIT
}
