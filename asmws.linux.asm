format ELF64 executable
entry _start

FDBUFFER_SIZE = 255

include 'syscalls/syscalls.linux.asm'

segment readable executable

include 'server/server.x.asm'

_start:
	call _main
	ExitSuccess


segment readable writable
include 'server/server.rw.asm'

segment readable
include 'server/server.ro.asm'
