
functions_str:
db "socket  "
db "bind    "
db "listen  "
db "fork    "
db "pipe    "

newline_str: db 10
.end:

initfail_str: db "Error while initializing: "
.end:

ready_str: db "Webserver ready.", 10
.end:

hex_str: db "0123456789ABCDEF"
.end:

align 8
saddr sockaddr_in6 PF_INET6, 8080, 0, 0, 16 dup(0)