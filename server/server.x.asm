
include "common.asm"

initfail:
	mov r10, rdi
	mov r9, rsi

	mov rdi, stdout
	mov rsi, initfail_str
	mov rdx, initfail_str.end-initfail_str
	Syscall SYS_WRITE

	mov rsi, r10
	mov rdx, r9 
	Syscall SYS_WRITE

	mov rsi, newline_str
	mov rdx, newline_str.end-newline_str
	Syscall SYS_WRITE
	ret

fail_socket:
	mov rdi, functions_str
	mov rsi, 8
	call initfail
	ExitFailure
fail_bind:
	mov rdi, functions_str+8
	mov rsi, 8
	call initfail
	ExitFailure	
fail_listen:
	mov rdi, functions_str+16
	mov rsi, 8
	call initfail
	ExitFailure
fail_fork:
	mov rdi, functions_str+24
	mov rsi, 8
	call initfail
	ExitFailure
fail_pipe:
	mov rdi, functions_str+32
	mov rsi, 8
	call initfail
	ExitFailure

_main:
	push rbp
	mov rbp, rsp

	sub rsp, 256 ; allocate some stack

	; Initialize server socket

	mov rdi, PF_INET6
	mov rsi, SOCK_STREAM
	xor rdx, rdx
	Syscall SYS_SOCKET
	cmp rax, -1
	je fail_socket
	mov [rbp-8], rax ; Server socket fd

	mov rdi, rax
	mov rsi, saddr
	mov rdx, sizeof_sockaddr_in6
	Syscall SYS_BIND
	test rax,rax
	jnz fail_bind

	mov rdi, [rbp-8]
	mov rsi, 60
	Syscall SYS_LISTEN
	test rax,rax
	jnz fail_listen

	; Drop privileges

	mov rdi, 65534
	mov rsi, rdi
	call drop_privileges

	; Fork off main process (become a child of init)

; 	Syscall SYS_FORK
; 	cmp rax, -1
; 	je fail_fork
; 	test rax, rax
; 	jz @f
; 	ExitSuccess
; @@:

	; Fork off worker process

	lea rdi, [rbp-16]
	Syscall SYS_PIPE
	test rax, rax
	jnz fail_pipe

	Syscall SYS_FORK
	cmp rax, -1
	je fail_fork
	test rax, rax
	jz worker
	mov [rbp-24], rax ; worker PID

	xor rdi, rdi
	mov edi, [rbp-16] ; pipe read end
	Syscall SYS_CLOSE

	mov rdi, stdout
	mov rsi, ready_str
	mov rdx, ready_str.end-ready_str
	Syscall SYS_WRITE

	push rbx
	push r12
	push r13

	mov rdi, (FDBUFFER_SIZE+1)*sizeof_pollfd
	call malloc
	mov rbx, rax ; poll buffer
	mov rdx, [rbp-8]
	mov [rbx], edx
	mov word [rbx+4], POLLPRI or POLLIN
	mov word [rbx+6], 0

	mov r12, 1 ; number of fds in poll buffer
	xor r13, r13 ; terminate flag
main_loop:

	mov rdi, rbx
	mov rsi, r12
	mov rdx, -1
	Syscall SYS_POLL
	cmp rax, -1
	jne @f
	ExitFailure
@@:
	mov rcx, rax
	mov r10, rbx
read_fds:
	mov ax, [r10+6]
	test ax, ax
	je .nodec

	mov word [r10+4], 0

	cmp rbx, r10
	je .accept

	; read from fd

	jmp .decr
.accept:
	xor rdi, rdi
	mov edi, [r10]
	xor rsi, rsi ; sockaddr* TODO
	xor rdx, rdx ; sockaddr_len* TODO
	Syscall SYS_ACCEPT
	test rax,rax
	; TODO error handling
	js .decr
	; TODO add fd to buffer

	regdebug rcx

.decr:
	dec rcx
.nodec:
	add r10, sizeof_pollfd
	test rcx, rcx
	jnz read_fds
.end:
	test r13, r13
	jz main_loop

	pop r13
	pop r12
	pop rbx

	leave
	ret

worker:
	xor rdi, rdi
	mov edi, [rbp-12] ; pipe write end
	Syscall SYS_CLOSE

	ExitSuccess