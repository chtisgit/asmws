
MALLOC_CANARY = 0xdeadbeef
MALLOC_MAX = 5*1024*1024 ; 5 MB

malloc_init_fail:
	ExitFailure

malloc_init:
	xor rdi, rdi
	mov rsi, MALLOC_MAX
	mov rdx, PROT_READ or PROT_WRITE
	mov r10, MAP_PRIVATE or MAP_ANONYMOUS
	mov r8, -1
	xor r9, r9
	Syscall SYS_MMAP
	cmp rax, MAP_FAILED
	je malloc_init_fail
	mov [malloc_storage], rax
	jz malloc.cont

malloc:
	mov rax, [malloc_storage]
	test rax,rax
	jz malloc_init
.cont:
	add rax, [malloc_index]
	mov rsi, rax
	add rsi, rdi
	mov dword [rsi], MALLOC_CANARY
	mov [rsi+4], rdi
	mov dword [rsi+12], MALLOC_CANARY
	add rdi, 16
	add [malloc_index], rdi
	ret

free:
	mov rax, [malloc_storage]
	mov rsi, rax
	; TODO fail if malloc_storage == 0
	add rax, [malloc_index]

	; TODO test canaries
	;cmp dword [rax-4], MALLOC_CANARY
	xor r10, r10 ; any non-freed regions found?
.search:
	mov rdx, [rax-12] ; size of last malloced segment
	test rdx, rdx
	jns .nonfreed
	neg rdx
	jmp @f
.nonfreed:
	inc r10
@@:
	sub rax, rdx
	cmp rax, rdi
	je .found

	cmp rax, rsi
	ja .search
	; not found:
	mov rax, 1
	ret

.found:
	add rax, rdx
	neg qword [rax-12]
	sub rax, rdx

	test r10, r10
	jnz @f
	; memory can be freed

	sub rax, rsi
	mov [malloc_index], rax
@@:
	xor rax, rax
	ret


; rdi ... gid
; rsi ... uid
drop_privileges:
	; check if we have privileges...
	Syscall SYS_GETGID
	test rax,rax
	jnz @f
	call .gid
@@:
	Syscall SYS_GETUID
	test rax,rax
	jnz @f
	mov rdi, rsi
	call .uid
@@:
	ret
.gid:
	Syscall SYS_SETREGID
	jmp .test
.uid:
	Syscall SYS_SETREUID
.test:
	test rax, rax
	jnz .fail
	ret
.fail:
	ExitFailure


macro regdebug reg {
	xchg rax, reg

	push rdi
	push rsi
	push rdx
	push rcx
	push rax 

	mov rcx, 16
 @@:
 	rol rax, 4
 	and rax, 0xF

;	mov rdi, stdout
;	lea rsi, [rax+hex_str]
;	mov rdx, 1
;	Syscall SYS_WRITE
; 	pop rax
; 	push rax
 	loop @b

; 	mov rdi, stdout
; 	mov rsi, newline_str
; 	mov rdx, newline_str.end-newline_str
; 	Syscall SYS_WRITE

	pop rax
	pop rcx
	pop rdx
	pop rsi
	pop rdi

	xchg rax, reg
}