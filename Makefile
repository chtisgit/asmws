
LINUXSRC = asmws.linux.asm server/server.ro.asm server/server.rw.asm server/server.x.asm
LINUXSRC += server/common.asm syscalls/syscalls.linux.asm

asmws.linux: $(LINUXSRC)
	fasm asmws.linux.asm
	chmod +x asmws.linux
